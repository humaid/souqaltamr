var express = require('express');
var app = express();
const MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');
var dateFormat = require("dateformat");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

// Connection URL
const url = `mongodb://ha82:abcha82354@mongo-server-1:27017/ha82`;

// Create a new MongoClient
const client = new MongoClient(url);

app.set('view engine', 'ejs');

app.use(express.static('static'))

app.get('/', function (req, res) {
  res.setHeader("Content-Type", "application/xhtml+xml")
  res.render('index', {"basket": "0"})
})

app.get('/country/:country', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    
    db.collection('Dates', function (err, collection) {
        
         collection.find({'country': req.params.country}).toArray(function(err, items) {
            if(err) throw err;    
           
            res.setHeader("Content-Type", "application/xhtml+xml")
            res.render('country', { "country": req.params.country ,"basket": "0", "dateslist": items})            
        });
        
    });
                
});
  
})

app.get('/buy/:item', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    if(err) throw err;
    db.collection('Dates', function (err, collection) {
        
        collection.findOne({'name': req.params.item},function(err, item) {
            if(err) throw err;    
           
  res.setHeader("Content-Type", "application/xhtml+xml")
            res.render('buy', { "item": req.params.item, "basket":"1", "date":item})           
        });
    }); 
});
})

app.post('/buy/:item', function(req,res){
 
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var customerOrder = { 
      name: req.body.name,
      email: req.body.email,
      address1: req.body.address1,
      address2: req.body.address2,
      postcode: req.body.postcode,
      country: req.body.country,
      phone: req.body.phone,
      date: req.body.date,
      doc: dateFormat(new Date(), "dd-mm-yyyy")
    };
    db.collection("CustomerOrders").insertOne(customerOrder, function(err, res2) {
      if (err) throw err;
      db.close();
      res.redirect('/orders');
    });
  });
 }) 
 
app.get('/orders', function (req, res) {
res.setHeader("Content-Type", "application/xhtml+xml")
  res.render('orders', {"basket":"0"})
})

app.get('/countries', function (req, res) {
res.setHeader("Content-Type", "application/xhtml+xml")
  res.render('countries', {"basket":"0"})
})
app.post('/quiz', function (req, res) {
  MongoClient.connect(url,function(err,db){
    if(err) throw err;

    db.collection('Dates', function(err,collection){
     var options = {'flavour':req.body.q1};
     if (req.body.q2 == "Yes") {
      options.country = "Saudi Arabia"
     }else {
      options.country = "Sultanate of Oman"
     }
     if (req.body.q3 == "np"){
       options.price = 5
     }
  

      collection.find(options).toArray(function (err, items){
        if(err) throw err;
        console.log(items)
        res.setHeader("Content-Type", "application/xhtml+xml")
        res.render('country', {"country":"your preference", "basket": "0", "dateslist": items})
      })
    })
  })
})

app.get('/quiz', function (req, res) {
  res.setHeader("Content-Type", "application/xhtml+xml")
    res.render('quiz', {"basket":"0"})
  })

app.post('/orders', function (req, res) {
  MongoClient.connect(url,function(err,db){
    if(err) throw err;

    db.collection('CustomerOrders', function(err,collection){
      collection.find({'phone': req.body.phone}).toArray(function (err, items){
        if(err) throw err;
    res.setHeader("Content-Type", "application/xhtml+xml")
        if(items.length==0){
          res.redirect('/orders?err=1')
          return
        }
        res.render('vorders', {"basket": "0", "orderslist": items})
      })
    })
  })
})

app.get('/policy', function (req, res) {
  res.setHeader("Content-Type", "application/xhtml+xml")
  res.render('policy', {"basket":"0"})
})

app.listen(3010, () => console.log('Example app listening on port 3010!'))
